@pancake.gg/logger
===

A logger that is clean and easy to use. Mostly designed for usage in the Pancake Discord bot project.

Installation
---

```
npm i @pancake.gg/logger
```

Example Usage
---

```js
const Logger = require('@pancake.gg/logger')

const logger = Logger.createLogger('TestLogger')

logger.info('Hello World!')
// 2019-05-26 23:00:00:0000 INFO  : Hello World!
```
