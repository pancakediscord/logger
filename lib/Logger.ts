import * as os from 'os';

import { Colors } from './util/Colors';
import { LevelColors, LogLevels, LogLevelValues } from './Constants';

const fecha = require('fecha');

export class Logger {
  public readonly name: string;
  public readonly options: LogOptions;

  private readonly colorer = new Colors();

  private constructor(name: string, options?: LogOptions) {
    this.name = name;

    this.options = {
      timestamps: true,
      eol: os.EOL,
      dateFormat: 'YYYY-MM-DD HH:mm:ss:SSS',
      logLevel: LogLevelValues.DEBUG
    };

    if (typeof options?.logLevel === 'string') {
      options.logLevel = LogLevelValues[options.logLevel];
    }

    Object.assign(this.options, options);
  }

  public static createLogger(name: string, options?: LogOptions) {
    const log = new Logger(name, options);
    return log;
  }

  get timestamp(): string {
    return this.options.timestamps ? fecha.format(Date.now(), this.options.dateFormat) : '';
  }

  private formatMessage(level: LogLevels, message: string) {
    const coloredLevel = this.colorer.get(LevelColors[level], level.padEnd(6));
    return `${this.timestamp} ${coloredLevel}: ${message}`;
  }

  public log(level: LogLevels, log: string | Error) {
    if (LogLevelValues[level] > this.options.logLevel) return;

    if (log instanceof Error) log = log.stack;
    const message = this.formatMessage(level, log);

    if (LogLevelValues[level] > LogLevelValues.ERROR) {
      if ((console as any)._stdout) {
        process.stdout.write(`${message}${this.options.eol}`);
      }
      else {
        // console.log adds a newline
        console.log(message);
      }
    }
    else if ((console as any)._stderr) {
      process.stderr.write(`${message}${this.options.eol}`);
    }
    else {
      // console.error adds a newline
      console.error(message);
    }
  }

  public info(log: string) {
    this.log(LogLevels.INFO, log);
  }

  public debug(log: string) {
    this.log(LogLevels.DEBUG, log);
  }

  public trace(log: string) {
    this.log(LogLevels.TRACE, log);
  }

  public warn(log: string) {
    this.log(LogLevels.WARN, log);
  }

  public error(log: string | Error) {
    this.log(LogLevels.ERROR, log);
  }

  public setLogLevel(level: LogLevels | LogLevelValues) {
    if (typeof level === 'string') {
      level = LogLevelValues[level];
    }

    this.options.logLevel = level;
  }
}

interface LogOptions {
  timestamps?: boolean
  dateFormat?: string
  eol?: string;
  logLevel?: LogLevels | LogLevelValues;
}
