export enum LevelColors {
  ERROR = 'red',
  WARN  = 'yellow',
  INFO  = 'cyan',
  DEBUG = 'magenta',
  TRACE = 'magenta'
}

export enum LogLevels {
  ERROR = 'ERROR',
  WARN  = 'WARN',
  INFO  = 'INFO',
  DEBUG = 'DEBUG',
  TRACE = 'TRACE'
}

export enum LogLevelValues {
  ERROR = 0,
  WARN  = 1,
  INFO  = 2,
  DEBUG = 3,
  TRACE = 4
}
