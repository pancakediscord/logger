import { Logger } from './Logger';
import { LogLevels, LogLevelValues } from './Constants';

export default Logger;
export { Logger, LogLevels, LogLevelValues };
